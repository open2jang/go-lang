# go-lang

# 2021-06-28 ~ 30: Go Language Study!!

# Install Go on Mac
1. brew update
2. brew install go
3. brew upgrade go

# If Not Upgrade go,
1. sudo -i
2. \rm -rf /usr/local/go/
3. replay 2, 3

# Install GoLan on Mac
1. Checking & Setting, preference>Go>GOROOT, GOPATH

# Setting GoLand(Jetbrain) IDE
1. Create new go project: go-lang
2. Create new main.go file
3. Write Code: hello world
4. build/run

# Call your code from External another module
1. mkdir greetings
2. cd greetings
3. go mod init example.com/greetings
4. Write greetings.go

package greetings

import "fmt"

// Hello returns a greeting for the named person.
func Hello(name string) string {
// Return a greeting that embeds the name in a message.
message := fmt.Sprintf("Hi, %v. Welcome!", name)
return message
}

5. cd ../
6. go mod init example.com/main
7. Modified main.go

package main

import (
"fmt"

    "example.com/greetings"
)

func main() {
// Get a greeting message and print it.
message := greetings.Hello("Gladys")
fmt.Println(message)
}

8. go mod edit -replace=example.com/greetings=../greetings
9. go mod tidy
10. vi go.mod 
    
module example.com/hello

go 1.16

replace example.com/greetings => ../greetings

require example.com/greetings v0.0.0-00010101000000-000000000000

11. go run .

# Compile and install the application
1. go build
2. go list -f '{{.Target}}'
4. go env -w GOBIN=/path/to/your/bin OR go env -w GOBIN=C:\path\to\your\bin
5. go install

